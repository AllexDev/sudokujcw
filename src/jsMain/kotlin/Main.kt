import org.jetbrains.compose.web.css.Style
import org.jetbrains.compose.web.renderComposable
import org.jetbrains.compose.web.renderComposableInBody
import presentation.Sudoku

fun main() {
    renderComposableInBody { Style(MainStyle) }
    renderComposable(rootElementId = "root") { Sudoku() }
}
