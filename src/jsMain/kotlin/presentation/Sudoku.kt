package presentation

import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import org.jetbrains.compose.web.attributes.InputType
import org.jetbrains.compose.web.attributes.maxLength
import org.jetbrains.compose.web.dom.Button
import org.jetbrains.compose.web.dom.Div
import org.jetbrains.compose.web.dom.Input
import org.jetbrains.compose.web.dom.Text
import presentation.SudokuViewModel.CellState.ValueState.DEFAULT
import presentation.SudokuViewModel.CellState.ValueState.SUCCESS
import presentation.SudokuViewModel.CellState.ValueState.WRONG

@Composable
fun Sudoku() {
    val viewModel by remember { mutableStateOf(SudokuViewModel()) }

    Div(attrs = { classes(SudokuStyle.content) }) {
        Field(viewModel)
        Menu(viewModel)
    }
}


/* MENU */

@Composable
private fun Menu(viewModel: SudokuViewModel) {
    Div(attrs = { classes(SudokuStyle.menu) }) {
        Button(attrs = { onClick { viewModel.onNewGameClick() } }) { Text("New Game") }
        Button(attrs = {
            if (viewModel.highlightEnabled) classes(SudokuStyle.active_button)
            onClick { viewModel.onChangeHighlightClick() }
        }) { Text("Highlights ${if (viewModel.highlightEnabled) "Enabled" else "Disabled"}") }
    }
}


/* FIELD */

@Composable
private fun Field(viewModel: SudokuViewModel) {
    val squaresIndices = viewModel.field.squaresIndices
    Div(attrs = { classes(SudokuStyle.field) }) { squaresIndices.forEach { Square(it, viewModel) } }
}

@Composable
private fun Square(id: Int, viewModel: SudokuViewModel) {
    val square = viewModel.field.square(id)
    Div(attrs = { classes(SudokuStyle.square) }) {
        square.indices.forEach { i -> CellHolder(square.toRealIndex(i), viewModel) }
    }
}

@Composable
private fun CellHolder(id: Int, viewModel: SudokuViewModel) {
    val isEditable by derivedStateOf { viewModel.field[id].isEditable }
    val isHighlight by derivedStateOf { viewModel.field[id].isHighlight }
    val mode by derivedStateOf { viewModel.field[id].valueState }

    Div(attrs = {
        classes(SudokuStyle.cell)
        if (isHighlight) classes(SudokuStyle.cell_highlight)
        if (isEditable) classes(SudokuStyle.cell_editable)
        when (mode) {
            DEFAULT -> Unit
            WRONG -> classes(SudokuStyle.cell_wrong)
            SUCCESS -> classes(SudokuStyle.cell_success)
        }
    }) {
        if (isEditable) EditableCell(id, viewModel)
        else FixedCell(id, viewModel)
    }
}

@Composable
private fun EditableCell(id: Int, viewModel: SudokuViewModel) {
    val fieldValue by derivedStateOf { viewModel.field[id].value }

    Input(InputType.Text) {
        maxLength(1)
        value(fieldValue.let { if (it == 0) "" else it.toString() })

        onClick { viewModel.onCellClick(id) }
        onKeyDown { if (viewModel.onCellValueChange(id, it.key)) it.preventDefault() }
        onInput { viewModel.onCellValueChange(id, it.value) }
    }
}

@Composable
private fun FixedCell(id: Int, viewModel: SudokuViewModel) {
    Text(viewModel.field[id].value.toString())
}
