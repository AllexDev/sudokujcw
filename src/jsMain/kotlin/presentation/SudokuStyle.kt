package presentation

import MainStyle
import generator.SudokuFieldDataView.Companion.SQUARE_IN_ROW_COL_COUNT
import generator.SudokuFieldDataView.Companion.SQUARE_ROW_COL_SIZE
import org.jetbrains.compose.web.css.AlignContent
import org.jetbrains.compose.web.css.AlignItems
import org.jetbrains.compose.web.css.Color
import org.jetbrains.compose.web.css.DisplayStyle
import org.jetbrains.compose.web.css.JustifyContent
import org.jetbrains.compose.web.css.LineStyle
import org.jetbrains.compose.web.css.StyleSheet
import org.jetbrains.compose.web.css.alignContent
import org.jetbrains.compose.web.css.alignItems
import org.jetbrains.compose.web.css.background
import org.jetbrains.compose.web.css.backgroundColor
import org.jetbrains.compose.web.css.border
import org.jetbrains.compose.web.css.color
import org.jetbrains.compose.web.css.cssRem
import org.jetbrains.compose.web.css.cursor
import org.jetbrains.compose.web.css.display
import org.jetbrains.compose.web.css.em
import org.jetbrains.compose.web.css.font
import org.jetbrains.compose.web.css.fontSize
import org.jetbrains.compose.web.css.fontWeight
import org.jetbrains.compose.web.css.gridTemplateColumns
import org.jetbrains.compose.web.css.gridTemplateRows
import org.jetbrains.compose.web.css.height
import org.jetbrains.compose.web.css.justifyContent
import org.jetbrains.compose.web.css.margin
import org.jetbrains.compose.web.css.marginTop
import org.jetbrains.compose.web.css.minWidth
import org.jetbrains.compose.web.css.padding
import org.jetbrains.compose.web.css.percent
import org.jetbrains.compose.web.css.px
import org.jetbrains.compose.web.css.textAlign
import org.jetbrains.compose.web.css.width

object SudokuStyle : StyleSheet(MainStyle) {

    private val HIGHLIGHT_COLOR = Color("#6272a4")

    val content by style {
        display(DisplayStyle.Flex)
        alignContent(AlignContent.Center)
        justifyContent(JustifyContent.Center)
    }

    val field by style {
        display(DisplayStyle.Grid)
        alignContent(AlignContent.Center)
        justifyContent(JustifyContent.Center)
        gridTemplateColumns("repeat($SQUARE_IN_ROW_COL_COUNT, ${100f / SQUARE_IN_ROW_COL_COUNT}%)")
        gridTemplateRows("repeat($SQUARE_IN_ROW_COL_COUNT, ${100f / SQUARE_IN_ROW_COL_COUNT}%)")
        fontSize(2.em)
        width(700.px)
        height(700.px)
    }

    val square by style {
        margin(8.px)
        display(DisplayStyle.Grid)
        alignContent(AlignContent.Center)
        justifyContent(JustifyContent.Center)
        gridTemplateColumns("repeat($SQUARE_ROW_COL_SIZE, ${100f / SQUARE_ROW_COL_SIZE}%)")
        gridTemplateRows("repeat($SQUARE_ROW_COL_SIZE, ${100f / SQUARE_ROW_COL_SIZE}%)")
    }

    const val cell_highlight = "highlight"

    const val cell_editable = "editable"

    const val cell_wrong = "wrong"

    const val cell_success = "success"

    val cell by style {
        margin(8.px)
        background("#44475a")
        display(DisplayStyle.Flex)
        alignItems(AlignItems.Center)
        justifyContent(JustifyContent.Center)
        color(Color.darkgray)
        (self + hover) style { background(HIGHLIGHT_COLOR.toString()) }

        (self + className(cell_highlight)) style { background(HIGHLIGHT_COLOR.toString()) }
        (self + className(cell_editable)) style { color(Color("#8be9fd")) }
        (self + className(cell_wrong)) style { color(Color("#ff5555")) }
        (self + className(cell_success)) style { color(Color("#13ff1c")) }

        "input" style {
            display(DisplayStyle.Block)
            padding(0.px)
            minWidth(0.px)
            height(100.percent)
            width(100.percent)
            background("none")
            textAlign("center")
            color(Color("inherit"))
            font("inherit")
            fontWeight("bold")
            fontSize(1.7.em)
            property("border", "none")
        }
    }

    const val active_button = "active"

    val menu by style {
        margin(8.px)

        val buttonSel = selector("button")
        buttonSel style {
            margin(8.px)
            width(12.5.cssRem)
            border(0.0625.cssRem, LineStyle.Solid, Color("#00a064cc"))
            padding(0.625.cssRem)
            backgroundColor(Color("#50fa7b33"))
            display(DisplayStyle.Block)
            color(Color("#f8f8f2"))
            cursor("pointer")
            property("text-transform", "uppercase")

            adjacent(self, buttonSel) style { marginTop(16.px) }
        }

        (buttonSel + className(active_button)) style { backgroundColor(Color("#bd93f933")) }
    }

}
