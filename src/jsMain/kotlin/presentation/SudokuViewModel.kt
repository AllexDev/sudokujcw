package presentation

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.runtime.toMutableStateList
import generator.SudokuFieldData
import generator.SudokuFieldDataView
import generator.SudokuGenerator
import generator.checkSolve
import presentation.SudokuViewModel.CellState.ValueState.DEFAULT
import presentation.SudokuViewModel.CellState.ValueState.SUCCESS
import presentation.SudokuViewModel.CellState.ValueState.WRONG
import kotlin.random.Random

class SudokuViewModel {

    data class CellState(
        val value: Int,
        val isEditable: Boolean = value == 0,
        val isHighlight: Boolean = false,
        val valueState: ValueState = DEFAULT
    ) {
        enum class ValueState {
            DEFAULT,
            WRONG,
            SUCCESS
        }
    }

    private class SudokuFieldState private constructor(
        private val stateList: SnapshotStateList<CellState>
    ) : SudokuFieldData<CellState>(stateList) {
        constructor() : this(List(SIZE) { CellState(0) }.toMutableStateList())

        fun locked(): SudokuFieldDataView<CellState> = SudokuFieldDataView(stateList.toList())
    }

    private val wrongs = SudokuFieldData { HashSet<Int>() }
    private val movesLeft = HashSet<Int>()
    private var affectedCells = emptySet<Int>()

    private val _field = SudokuFieldState()
    val field: SudokuFieldDataView<CellState> = _field

    private val _highlightEnabled = mutableStateOf(false)
    val highlightEnabled by _highlightEnabled

    init {
        _field.indices.forEach(movesLeft::add)
    }

    fun onNewGameClick() {
        newGame()
    }

    fun onChangeHighlightClick() {
        setHighlightEnabled(!_highlightEnabled.value)
    }

    fun onCellValueChange(id: Int, newValue: String): Boolean = newValue
        .let { if (it.isEmpty()) 0 else it.toIntOrNull() }
        ?.also { updateCellValue(id, it) } != null

    fun onCellClick(cellId: Int) {
        val affected = _field.affectedCellsIndexes(cellId)

        if (_highlightEnabled.value) {
            affectedCells.subtract(affected).forEach { _field[it] = _field[it].copy(isHighlight = false) }
            affected.forEach { _field[it] = _field[it].copy(isHighlight = true) }
        }
        affectedCells = affected
    }

    private fun newGame() {
        movesLeft.clear()
        affectedCells = emptySet()
        wrongs.forEach(HashSet<Int>::clear)

        val newField = SudokuGenerator.generate()
        newField.forEachIndexed { index, value ->
            val cellValue = if (Random.nextInt() % 3 == 0) movesLeft.add(index).let { 0 } else value
            _field[index] = CellState(cellValue)
        }
    }

    private fun setHighlightEnabled(enable: Boolean) {
        _highlightEnabled.value = enable
        val lockedField = _field.locked()
        if (enable) {
            lockedField.forEachIndexed { i, cell ->
                val isWrong = wrongs[i].isNotEmpty()
                val isHighlight = i in affectedCells
                if (!isWrong && !isHighlight) return@forEachIndexed

                _field[i] = cell.copy(isHighlight = isHighlight, valueState = if (isWrong) WRONG else DEFAULT)
            }
        } else {
            lockedField.forEachIndexed { i, cell ->
                if (!cell.isHighlight && cell.valueState == DEFAULT) return@forEachIndexed

                _field[i] = cell.copy(isHighlight = false, valueState = DEFAULT)
            }
        }
    }

    private fun updateCellValue(cellId: Int, newValue: Int) {
        if (_field[cellId].value == newValue) return

        _field[cellId] = _field[cellId].copy(value = newValue, valueState = processWrongsInput(cellId, newValue))
        if (newValue == 0) movesLeft.add(cellId) else movesLeft.remove(cellId)
        if (movesLeft.isEmpty()) checkSolve()
    }

    private fun processWrongsInput(cellId: Int, value: Int): CellState.ValueState {
        val isHighlight = _highlightEnabled.value

        val lockedField = _field.locked()
        val wrongsCells = HashSet<Int>()
        for (idx in affectedCells) {
            if (idx == cellId) continue

            val cell = lockedField[idx]
            if (cell.value > 0) {
                if (cell.value == value) {
                    wrongs[idx].add(cellId)
                    wrongsCells.add(idx)
                } else {
                    wrongs[idx].remove(cellId)
                }
            }
            if (isHighlight) _field[idx] = cell.copy(valueState = if (wrongs[idx].isNotEmpty()) WRONG else DEFAULT)
        }
        if (wrongsCells.isNotEmpty()) wrongs[cellId].addAll(wrongsCells) else wrongs[cellId].clear()

        return if (isHighlight && wrongsCells.isNotEmpty()) WRONG else DEFAULT
    }

    private fun checkSolve() {
        val lockedField = _field.locked()
        val isSolved = lockedField.checkSolve(CellState::value).isEmpty()
        lockedField.forEachIndexed { i, cell ->
            _field[i] = cell.copy(
                isEditable = false,
                valueState = when {
                    isSolved -> SUCCESS
                    wrongs[i].isNotEmpty() -> WRONG
                    else -> DEFAULT
                }
            )
        }
    }

}
