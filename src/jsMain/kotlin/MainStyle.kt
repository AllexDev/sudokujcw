import org.jetbrains.compose.web.css.StyleSheet
import org.jetbrains.compose.web.css.background

object MainStyle : StyleSheet() {

    init {
        "body" {
            background("#282a36")
        }
    }

}
