package generator

import generator.SudokuFieldDataView.Companion.BLOCK_SIZE
import kotlin.properties.Delegates
import kotlin.random.Random

typealias SudokuField = SudokuFieldDataView<Int>

object SudokuGenerator {

    private const val NO_VALUE = -1

    fun generate(random: Random = Random): SudokuField {
        var result: SudokuFieldData<CellData>? = null
        while (result == null) {
            val field = SudokuFieldData(::CellData)
            kotlin.runCatching {
                for (squareIndex in field.squaresIndices) field.fill(random) { square(squareIndex) }
                result = field
            }
        }
        return SudokuFieldDataView(result!!.map(CellData::value))
    }

    private fun SudokuFieldData<CellData>.fill(
        random: Random,
        cells: SudokuFieldData<CellData>.() -> CellList<CellData>
    ) {
        val possibleCells = PossibleCellList(cells())
        while (!possibleCells.isEmpty()) {
            val cell = possibleCells.poll()
            cell.value = cell.possibleValues.random(random)
            updateAffectedCells(cell)
            possibleCells.invalidate()
        }
    }

    private fun SudokuFieldData<CellData>.updateAffectedCells(cell: CellData, newValue: Int = cell.value) {
        val square = squareByCellIndex(cell.idx)
        val row = rowByCellIndex(cell.idx)
        val col = colByCellIndex(cell.idx)

        for (i in 0 until square.size) {
            square[i].possibleValues.remove(newValue)
            row[i].possibleValues.remove(newValue)
            col[i].possibleValues.remove(newValue)
        }
    }

    private class CellData(val idx: Int) {
        val possibleValues = PossibleValues()
        var value: Int by Delegates.observable(NO_VALUE) { _, _, newValue -> possibleValues.remove(newValue) }
        val hasValue: Boolean get() = value > NO_VALUE

        override fun toString(): String {
            return (value.takeIf { it > NO_VALUE } ?: possibleValues).toString()
        }

        class PossibleValues {
            private val values = IntArray(BLOCK_SIZE) { it + 1 }
            private val indexes = IntArray(BLOCK_SIZE) { it }
            private var realSize = BLOCK_SIZE

            val size get() = realSize

            fun random(random: Random = Random): Int {
                require(realSize > 0) { "No possible values" }
                return values[random.nextInt(realSize)]
            }

            fun remove(value: Int) {
                val index = indexes[value - 1].takeIf { it >= 0 } ?: return

                realSize--
                indexes[values[index] - 1] = NO_VALUE
                values[index] = values[realSize]
                values[realSize] = 0
                if (values[index] > 0) indexes[values[index] - 1] = index
            }

            override fun toString(): String = values.takeIf { realSize > 0 }
                ?.asList()?.subList(0, realSize)
                ?.joinToString(separator = ",", prefix = "[", postfix = "]")
                ?: "[*]"
        }
    }

    private class PossibleCellList(cellList: CellList<CellData>) {
        private class Node(val cell: CellData, var next: Node? = null)

        private var head: Node? = null
        private var size = 0

        init {
            var minPossibleSize = Int.MAX_VALUE
            var tail: Node? = null
            for (cellData in cellList) {
                if (cellData.hasValue) continue

                if (cellData.possibleValues.size < minPossibleSize) {
                    head = Node(cellData, head)
                    if (tail == null) tail = head
                    minPossibleSize = cellData.possibleValues.size
                } else {
                    tail = Node(cellData).also { tail?.next = it }
                }
                size++
            }
        }

        fun isEmpty(): Boolean = size == 0

        fun poll(): CellData {
            val result = head!!.cell
            head = head?.next
            size--
            return result
        }

        fun invalidate() {
            var minPossibleSize = Int.MAX_VALUE
            var current = head
            var prevNode: Node? = null
            while (current != null) {
                if (current.cell.hasValue) {
                    if (prevNode == null) {
                        head = current.next
                    } else {
                        prevNode.next = current.next
                        current = prevNode
                    }
                    size--
                } else if (current.cell.possibleValues.size < minPossibleSize) {
                    minPossibleSize = current.cell.possibleValues.size
                    if (prevNode != null) {
                        prevNode.next = current.next
                        current.next = head
                        head = current
                        current = prevNode
                    }
                }
                prevNode = current
                current = current.next
            }
        }
    }

}
