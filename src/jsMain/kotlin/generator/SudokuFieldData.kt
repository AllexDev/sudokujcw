package generator

interface CellList<T> : Iterable<T> {
    val size: Int
    val indices: IntRange

    operator fun get(index: Int): T
}

interface MutableCellList<T> : CellList<T> {
    operator fun set(index: Int, value: T): T
}

open class SudokuFieldDataView<T>(protected val srcData: List<T>) : CellList<T> {

    companion object {
        const val SIZE = 81
        const val BLOCK_SIZE = 9 // elements size for row, col, square
        const val BLOCK_COUNT = 9 // count of rows, cols, squares
        const val SQUARE_ROW_COL_SIZE = 3
        const val SQUARE_IN_ROW_COL_COUNT = 3
    }

    init {
        check(srcData.size == SIZE) { "Data size must be $SIZE" }
    }

    private val cachedRows = arrayOfNulls<RowView>(BLOCK_COUNT)
    private val cachedCols = arrayOfNulls<ColView>(BLOCK_COUNT)
    private val cachedSquares = arrayOfNulls<SquareView>(BLOCK_COUNT)

    final override val size: Int = SIZE
    final override val indices: IntRange = 0 until SIZE
    val squaresIndices: IntRange = 0 until BLOCK_COUNT
    val rowsIndices: IntRange = 0 until BLOCK_COUNT
    val colsIndices: IntRange = 0 until BLOCK_COUNT

    override fun get(index: Int): T = srcData[index]
    operator fun get(row: Int, col: Int): T = row(row)[col]

    open fun square(index: Int): SquareView = getCachedSquaresOrPut(index, ::SquareView)
    open fun squareByCellIndex(cellIndex: Int): SquareView = square(cellIndex.squareIndex)

    open fun row(index: Int): RowView = getCachedRowOrPut(index, ::RowView)
    open fun rowByCellIndex(cellIndex: Int): RowView = row(cellIndex.rowIndex)

    open fun col(index: Int): ColView = getCachedColOrPut(index, ::ColView)
    open fun colByCellIndex(cellIndex: Int): ColView = col(cellIndex.colIndex)

    fun affectedCellsIndexes(cellIndex: Int): Set<Int> {
        val affected = hashSetOf<Int>()
        val squareData = squareByCellIndex(cellIndex)
        val rowData = rowByCellIndex(cellIndex)
        val colData = colByCellIndex(cellIndex)

        for (index in 0 until BLOCK_SIZE) {
            affected.add(squareData.toRealIndex(index))
            affected.add(rowData.toRealIndex(index))
            affected.add(colData.toRealIndex(index))
        }

        return affected
    }

    override fun iterator(): Iterator<T> = IteratorImpl(this)

    override fun toString(): String {
        val stringFiled = StringBuilder()
        for (row in rowsIndices) {
            for (col in colsIndices) {
                stringFiled.append(this[row, col]).append(" ")
                if ((col + 1) % 3 == 0) stringFiled.append(" ")
            }
            stringFiled.appendLine()
            if ((row + 1) % 3 == 0) stringFiled.appendLine()
        }

        return stringFiled.toString()
    }

    protected inline val Int.squareIndex: Int
        get() = this / BLOCK_COUNT
    protected inline val Int.rowIndex: Int
        get() = squareIndex.let { it / SQUARE_IN_ROW_COL_COUNT * SQUARE_ROW_COL_SIZE + (this - it * BLOCK_SIZE) / SQUARE_ROW_COL_SIZE }
    protected inline val Int.colIndex: Int
        get() = (squareIndex % SQUARE_IN_ROW_COL_COUNT) * SQUARE_ROW_COL_SIZE + this % SQUARE_ROW_COL_SIZE

    @Suppress("UNCHECKED_CAST")
    protected fun <R : SquareView> getCachedSquaresOrPut(index: Int, block: (Int) -> R): R {
        return (cachedSquares[index] ?: block(index).also { cachedSquares[index] = it }) as R
    }

    @Suppress("UNCHECKED_CAST")
    protected fun <R : RowView> getCachedRowOrPut(index: Int, block: (Int) -> R): R {
        return (cachedRows[index] ?: block(index).also { cachedRows[index] = it }) as R
    }

    @Suppress("UNCHECKED_CAST")
    protected fun <R : ColView> getCachedColOrPut(index: Int, block: (Int) -> R): R {
        return (cachedCols[index] ?: block(index).also { cachedCols[index] = it }) as R
    }

    @Suppress("NOTHING_TO_INLINE")
    protected inline fun checkBounds(i: Int, indices: IntRange): Int {
        check(i in indices) { "Index out of bound($i !in $indices)" }
        return i
    }

    open inner class SquareView(val squareIndex: Int) : CellList<T> {
        protected val startIndex: Int = squareIndex * BLOCK_SIZE
        protected inline val Int.realIndex: Int
            get() = startIndex + this

        final override val size: Int = BLOCK_SIZE
        final override val indices: IntRange = 0 until size

        override fun get(index: Int): T = this@SudokuFieldDataView[checkBounds(index, indices).realIndex]
        override fun iterator(): Iterator<T> = IteratorImpl(this)
        fun toRealIndex(localIndex: Int): Int = localIndex.realIndex
    }

    open inner class RowView(val rowIndex: Int) : CellList<T> {
        protected val startIndex = (rowIndex / SQUARE_IN_ROW_COL_COUNT).let { squareRow ->
            squareRow * 27 + (rowIndex - squareRow * SQUARE_ROW_COL_SIZE) * SQUARE_ROW_COL_SIZE
        }
        protected inline val Int.realIndex: Int
            get() = startIndex + this + this / SQUARE_ROW_COL_SIZE * 6

        final override val size: Int = BLOCK_SIZE
        final override val indices: IntRange = 0 until size

        override fun get(index: Int): T = this@SudokuFieldDataView[checkBounds(index, indices).realIndex]
        override fun iterator(): Iterator<T> = IteratorImpl(this)
        fun toRealIndex(localIndex: Int): Int = localIndex.realIndex
    }

    open inner class ColView(val colIndex: Int) : CellList<T> {
        protected val startIndex = (colIndex / SQUARE_IN_ROW_COL_COUNT).let { squareCol ->
            squareCol * 9 + (colIndex - squareCol * SQUARE_ROW_COL_SIZE)
        }
        protected inline val Int.realIndex: Int
            get() = startIndex + this * SQUARE_ROW_COL_SIZE + this / SQUARE_IN_ROW_COL_COUNT * 18

        final override val size: Int = BLOCK_SIZE
        final override val indices: IntRange = 0 until size

        override fun get(index: Int): T = this@SudokuFieldDataView[checkBounds(index, indices).realIndex]
        override fun iterator(): Iterator<T> = IteratorImpl(this)
        fun toRealIndex(localIndex: Int): Int = localIndex.realIndex
    }

    private class IteratorImpl<T>(private val list: CellList<T>) : Iterator<T> {
        private var index = 0
        override fun hasNext(): Boolean = index < list.size
        override fun next(): T = if (hasNext()) list[index++] else throw NoSuchElementException(index.toString())
    }

}

open class SudokuFieldData<T>(
    protected val mutableSrcData: MutableList<T>
) : SudokuFieldDataView<T>(mutableSrcData), MutableCellList<T> {

    constructor(initCell: (Int) -> T) : this(MutableList(SIZE, initCell))

    override fun set(index: Int, value: T): T = mutableSrcData.set(index, value)
    operator fun set(row: Int, col: Int, value: T): T = row(row).set(col, value)

    override fun square(index: Int): MutableSquare = getCachedSquaresOrPut(index, ::MutableSquare)
    override fun squareByCellIndex(cellIndex: Int): MutableSquare = square(cellIndex.squareIndex)

    override fun row(index: Int): MutableRow = getCachedRowOrPut(index, ::MutableRow)
    override fun rowByCellIndex(cellIndex: Int): MutableRow = row(cellIndex.rowIndex)

    override fun col(index: Int): MutableCol = getCachedColOrPut(index, ::MutableCol)
    override fun colByCellIndex(cellIndex: Int): MutableCol = col(cellIndex.colIndex)

    inner class MutableSquare(squareIndex: Int) : SquareView(squareIndex), MutableCellList<T> {
        override fun set(index: Int, value: T): T {
            return this@SudokuFieldData.set(checkBounds(index, indices).realIndex, value)
        }
    }

    inner class MutableRow(rowIndex: Int) : RowView(rowIndex), MutableCellList<T> {
        override fun set(index: Int, value: T): T {
            return this@SudokuFieldData.set(checkBounds(index, indices).realIndex, value)
        }
    }

    inner class MutableCol(colIndex: Int) : ColView(colIndex), MutableCellList<T> {
        override fun set(index: Int, value: T): T {
            return this@SudokuFieldData.set(checkBounds(index, indices).realIndex, value)
        }
    }

}

fun <T> SudokuFieldDataView<T>.checkSolve(transformer: (T) -> Int): Set<Int> {
    val errors = hashSetOf<Int>()
    val rowMatrix = SudokuFieldData { false }
    val colMatrix = SudokuFieldData { false }
    val squareMatrix = SudokuFieldData { false }

    for (index in indices) {
        val cel = this[index]
        val c = transformer(cel) - 1
        if (c < 1) continue

        val rowCells = rowMatrix.rowByCellIndex(index)
        val colCells = colMatrix.colByCellIndex(index)
        val squareCells = squareMatrix.squareByCellIndex(index)

        val errorFrom = when {
            rowCells[c] -> rowByCellIndex(index)
            colCells[c] -> colByCellIndex(index)
            squareCells[c] -> squareByCellIndex(index)
            else -> null
        }?.indexOfFirst { transformer(it) - 1 == c }

        if (errorFrom != null) {
            errors.add(errorFrom)
            errors.add(index)
        }

        rowCells[c] = true
        colCells[c] = true
        squareCells[c] = true
    }

    return errors
}
